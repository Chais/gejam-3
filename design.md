# Theme
Duality - Dreams/Nightmares

# Ideas
- Platformer
- Player periodically drops breadcrumbs
- Start in dream (tileset 1, breadcrumbs invisible)
- Front of next tileset sweeps level left to right, breadcrumbs visible in nightmare tilesets
- When penultimate wave overtakes player, level rotates 90° ccw, turning progression into bottom-up
- When last wave overtakes player, sanity starts waning
- Picking up breadcrumbs restores some sanity, running out of sanity restarts level
- Reaching a checkpoint, resets tileset, clears breadcrumbs, resets sanity

# Mechanics
- Walk & Run
- Jump

class_name Player
extends Actor


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var left_pressed = 0
var right_pressed = 0
var walk_scale = 0
var sprinting = false

# Exported variables are accessible via the inspector
export(float) var walk_accel = 80  # m/s^2
export(float) var sprint_accel = 160
export(float) var walk_speed = 80
export(float) var sprint_speed = 120


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var accel = sprint_accel if sprinting else walk_accel
	var max_speed = sprint_speed * max(left_pressed, right_pressed) if\
			sprinting else walk_speed * walk_scale
	# _velocity.x = right_pressed * accel * delta - left_pressed * accel * delta
	_velocity.x += (right_pressed - left_pressed) * accel * delta
	if _velocity.x > max_speed:
		_velocity.x = max_speed
	if _velocity.x < -max_speed:
		_velocity.x = -max_speed
	_velocity = move_and_slide(_velocity, Vector2.UP)


# Blatantly stole from godot-demo-projects
# This function calculates a new velocity whenever you need it.
# It allows you to interrupt jumps.
func calculate_move_velocity(
		linear_velocity, direction, speed, is_jump_interrupted):
	var velocity = linear_velocity
	velocity.x = speed.x * direction.x
	if direction.y != 0.0:
		velocity.y = speed.y * direction.y
	if is_jump_interrupted:
		# Decrease the Y velocity by multiplying it, but don't set it to 0
		# as to not be too abrupt.
		velocity.y *= 0.6
	return velocity


# Called whenever an unhandled input key makes its way to the Player
func _unhandled_key_input(event):
	var root = get_tree().get_root()
	if InputMap.event_is_action(event, "left"):
		# Input.get_action_strength takes care of analog sticks
		left_pressed = 1 if event.pressed else 0
		walk_scale = Input.get_action_strength("left")
		root.set_input_as_handled()  # Consume event to prevent further handling
		return
	if InputMap.event_is_action(event, "right"):
		right_pressed = 1 if event.pressed else 0
		walk_scale = Input.get_action_strength("right")
		root.set_input_as_handled()
		return
	if InputMap.event_is_action(event, "sprint"):
		sprinting = event.pressed
		root.set_input_as_handled()
		return
